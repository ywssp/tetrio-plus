/*
  This has to be an injected script and not a content script because
  DOM events appear to stringify objects passed to them only for content
  scripts, and I need to call a function on an event's detail object
*/
(async () => {
  [...document.getElementsByClassName('tetrio-plus-osd')].forEach(c => c.remove());
  const { iconSet, baseIconURL } = await new Promise(res => {
    window.addEventListener(
      "baseIconURL",
      evt => {
        // json stringify/parse to prevent
        // `Permission denied to access property "then"`
        // even though you can send objects through
        res(JSON.parse(evt.detail));
      },
      { once: true }
    );
    window.dispatchEvent(new CustomEvent("getBaseIconURL"));
  });

  let osd = document.createElement('div');
  osd.classList.toggle('tetrio-plus-osd', true);
  osd.classList.toggle('icon-set-' + iconSet, true);
  document.body.appendChild(osd);

  let buttons = [];
  let buttonMap = {};
  function button(name, tetrioName) {
    let elem = document.createElement('div');
    elem.classList.toggle('tetrio-plus-osd-key', true);
    elem.classList.toggle(name, true);
    elem.style.gridArea = name;

    let icon = `url("${baseIconURL}icon-${tetrioName}.png")`;
    elem.style.setProperty('background-image', icon);
    elem.setActive = function(active) {
      if (iconSet == 'old') {
        elem.classList.toggle('active', active);
      } else {
        let suffix = active ? '-pressed' : '';
        let icon = `url("${baseIconURL}icon-${tetrioName}${suffix}.png")`;
        elem.style.setProperty('background-image', icon);
      }
    }

    elem.setActive(false);
    osd.appendChild(elem);
    buttons.push(elem);
    buttonMap[tetrioName] = elem;
  }

  button('left', 'moveLeft');
  button('right', 'moveRight');
  button('softdrop', 'softDrop');
  button('harddrop', 'hardDrop');
  button('spin-cw', 'rotateCW');
  button('spin-ccw', 'rotateCCW');
  button('spin-180', 'rotate180');
  button('hold', 'hold');

  let handleContainer = document.createElement('div');
  handleContainer.classList.toggle('tetrio-plus-osd-handle-container');
  osd.appendChild(handleContainer);

  let resizeHandle = document.createElement('div');
  resizeHandle.classList.toggle('tetrio-plus-osd-resize-handle');
  handleContainer.appendChild(resizeHandle);

  if (iconSet == 'old') {
    resizeHandle.innerText = '🡦';
  } else {
    let resizeIcon = `url("${baseIconURL}resize.png")`;
    resizeHandle.style.setProperty('background-image', resizeIcon);
  }


  // too lazy to write hooks back to browser storage from an injected script
  const fixedAspect = 3.125;
  const minWidth = 150;
  const minHeight = minWidth / fixedAspect;
  let x = +localStorage.tp_osd_x || 40;
  let y = +localStorage.tp_osd_y || 40;
  let w = +localStorage.tp_osd_w || minWidth * 2;
  let h = +localStorage.tp_osd_h || minHeight * 2;

  function resize() {
    let _x = x, _y = y, _w = w, _h = h;
    if (_w < minWidth) _w = minWidth;
    if (_h < minHeight) _h = minHeight;

    osd.style.left = _x + 'px';
    osd.style.top = _y + 'px';
    osd.style.width = _w + 'px';
    osd.style.height = _h + 'px';

    let fontSize = Math.min(Math.floor(_w/6), Math.floor(_h/2)) - 4;
    for (let button of buttons) {
      button.style.fontSize = fontSize + 'px';
    }
  }
  resize();

  let dragging = false;
  let resizing = false;
  let lastMouseX = 0, lastMouseY = 0;
  osd.addEventListener('mousedown', evt => {
    lastMouseX = evt.clientX;
    lastMouseY = evt.clientY;
    dragging = true;
  });
  resizeHandle.addEventListener('mousedown', evt => {
    lastMouseX = evt.clientX;
    lastMouseY = evt.clientY;
    resizing = true;
  });
  document.body.addEventListener('mousemove', evt => {
    if (resizing) {
      w += evt.clientX - lastMouseX;
      h += evt.clientY - lastMouseY;
      if (evt.shiftKey) h = w / fixedAspect;
      resize();
    } else if (dragging) {
      x += evt.clientX - lastMouseX;
      y += evt.clientY - lastMouseY;
      resize();
    }
    lastMouseX = evt.clientX;
    lastMouseY = evt.clientY;
  });
  document.body.addEventListener('mouseup', evt => {
    dragging = false;
    resizing = false;
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (w < minWidth) w = minWidth;
    if (h < minHeight) h = minHeight;
    if (x > window.innerWidth - w) x = window.innerWidth - w;
    if (y > window.innerHeight - h) y = window.innerHeight - h;
    if (w > window.innerWidth) w = window.innerWidth;
    if (h > window.innerHeight) w = window.innerHeight;
    localStorage.tp_osd_x = x;
    localStorage.tp_osd_y = y;
    localStorage.tp_osd_w = w;
    localStorage.tp_osd_h = h;
    resize();
  });

  let games = [];
  document.addEventListener('tetrio-plus-on-game', evt => {
    // If a game has a socket, its someone else's board
    // Singleplayer, replay, and own boards in multiplayer have no sockets
    if (evt.detail.socket()) return;

    let game = evt.detail;
    games.push(game);

    game.id = Date.now();

    function dropGame(game) {
      if (games.includes(game)) {
        games.splice(games.indexOf(game), 1);
        game.unbind(game.__tp_onstart);
        console.log("Dropped game", game.id);
      }
    }
    function onEvent(evt) {
      game.__tp_lastevent = Date.now();

      if (evt.type == 'start') {
        buttons.forEach(el => el.setActive(false));
        for (let othergame of games.slice()) {
          if (Date.now() < othergame.__tp_lastevent + 10000) continue;
          dropGame(othergame);
        }
      }

      if (evt.type == 'end') {
        buttons.forEach(el => el.setActive(false));
        dropGame(game);
        return;
      }

      let elem = buttonMap[evt.data.key];
      if (!elem) return;

      switch (evt.type) {
        case 'keyup':
          elem.setActive(false);
          break;
        case 'keydown':
          elem.setActive(true);
          break;
      }
    }

    game.bind(onEvent);
    game.__tp_onstart = onEvent;
    game.__tp_lastevent = Date.now();
  });
})().catch(ex => console.error(ex));
